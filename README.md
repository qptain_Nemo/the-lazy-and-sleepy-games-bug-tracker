# the Lazy and Sleepy bug tracker #

This is the bug tracker for [Lazy and Sleepy games](https://lazyandsleepy.org). Report any issues here using the [issues section](https://bitbucket.org/qptain_Nemo/the-lazy-and-sleepy-games-bug-tracker/issues).

### helpful information ###

* [Markdown syntax](https://bitbucket.org/tutorials/markdowndemo)